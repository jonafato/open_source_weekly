+++
title = "About"
type = "page"
date = 2019-02-05T01:42:42+02:00
+++

## Author

This letter is lovingly curated by hand by
<a href="https://kerkour.fr" rel="noopener" target="_blank">Sylvain Kerkour</a>, the creator of
<a href="https://bloom.sh" target="_blank" rel="noopener">
Bloom: An open source and encrypted productivity app (Drive, Calendar, Contacts...)</a>.

<br />

## Contributing

All suggestions are welcome (as long as it's not a disguised ad)! You can contribute either by opening a
<a href="{{< giturl >}}/issues" target="_blank" rel="noopener">ticket on Gitlab</a> or using the email below.

<br />

## Contact

Fell free to contact me by email: {{< email >}}

<br />

## Mastodon

Follow
<a href="https://social.kerkour.fr/@Sylvain" target="_blank" rel="noopener">@sylvain@kerkour.fr</a> on the Fediverse to never miss an issue.

<br />

## Twitter

Follow
<a href="https://twitter.com/@42Bloom" target="_blank" rel="noopener">@42Bloom</a> on Twitter to never miss an issue.

<br />

## RSS

Prefer RSS? <a href="{{< rssurl >}}">Use this URL in your favorite RSS reader</a>.

<br />


## License

All the content on this website and the issues are licensed under the
<a rel="noopener" target="_blank" href="https://creativecommons.org/licenses/by-sa/4.0/" >CC BY-SA 4.0 License</a>.

<br />

## Source code

The source code of this website is available on GitLab:
<a href="{{< giturl >}}" target="_blank" rel="noopener">{{< giturl >}}</a>
